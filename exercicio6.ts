/*
Crie um algoritmo que solicite três números do usuário e exiba o maior deles.
*/

namespace exercicio_6
{
    const numero1 = 10
    const numero2 = 3090459304
    const numero3 = 1267

    if (numero1 > numero2 && numero1 > numero3)
    {
        console.log(`O maior numero é: ${numero1}`)
    }
    else if (numero2 > numero1 && numero2 > numero3)
    {
        console.log(`O maior numero é: ${numero2}`)
    }
    else if (numero3 > numero1 && numero3 > numero2)
    {
        console.log(`O maior numero é: ${numero3}`)
    }
}