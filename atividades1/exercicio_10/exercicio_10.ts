/* Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R2 */

namespace exercicio_10
{
    let pi, rQuad, r, area_circulo: number;

    pi = 3,141592653; 
    r = 4;

    rQuad = r ** 2;

    area_circulo = pi * rQuad;

    console.log(`A área do círculo é de: ${area_circulo}`);
}