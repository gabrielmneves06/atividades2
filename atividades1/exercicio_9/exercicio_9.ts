/* Faça um programa que calcule e mostre a área de um
triângulo. Sabe-se que: Área = (base * altura)/2. */

namespace exercicio_9
{
    let base, altura, area_triangulo: number;

    base = 10;
    altura = 7;

    area_triangulo = (base * altura) / 2;
    
    console.log(` A área desse triângulo será de: ${area_triangulo}`)
}